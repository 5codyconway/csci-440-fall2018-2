# Team Members
* Cody Conway


# Idea 1 (Relational Algebra - Select, Project, Join)
The tutorial with the above title will pursue the objective of exploring the 
segments of relational algebra defined by the three operations mentioned. Hopefully, 
this will provide an introductory understanding of some of the fundamental 
operations of relational algebra. The applications of each of the operations 
mentioned will be invaluable to anyone who uses relational algebra. 

# Idea 2 (ER Model - Relationships)
This tutorial would explain how to describe relationships within an ER model; 
this provides a clear and succinct explanation of the ER model and entities 
connect to each other. The tutorial, ideally, helps readers with their general 
understanding of the ER model, specifically, in reference to its relationships.

# Idea 3 (ER Model - Functional Dependencies)
To elucidate the connections between keys and attributes within an ER model, 
this tutorial makes apparent what it means to be functionally dependent, 
both partially and completely. The tutorial, ideally, helps readers with 
their general understanding of the ER model, specifically, in reference to 
its functional dependencies. 
